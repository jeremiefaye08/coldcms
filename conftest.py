import pytest
from django.db.models.signals import (
    m2m_changed,
    post_delete,
    post_save,
    pre_delete,
    pre_save,
)
from wagtail.core.signals import page_published, page_unpublished

app_signals = [
    pre_save,
    post_save,
    pre_delete,
    post_delete,
    m2m_changed,
    page_published,
    page_unpublished,
]


@pytest.fixture(autouse=True)  # Automatically use in tests.
def mute_signals(request):
    # Skip applying, if marked with `enabled_signals`
    if "enable_signals" in request.keywords:
        return
    restore = {}
    for signal in app_signals:
        # Temporally remove the signal's receivers (a.k.a attached functions)
        restore[signal] = signal.receivers
        signal.receivers = []

    def restore_signals():
        # When the test tears down, restore the signals.
        for signal, receivers in restore.items():
            signal.receivers = receivers

    # Called after a test has finished.
    request.addfinalizer(restore_signals)


@pytest.fixture(autouse=True)
def delete_site_cache(request):
    from django.core.cache import cache

    cache.delete("wagtail_site_root_paths")


@pytest.fixture(autouse=True)
def signals_dont_user_cache(request):
    for signal in app_signals:
        signal.use_caching = False
