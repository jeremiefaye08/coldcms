import datetime

import pytest
from coldcms.blog import views
from coldcms.blog.models import (
    BlogAuthorIndexPage,
    BlogDateIndexPage,
    BlogIndexPage,
    BlogListAuthorsIndexPage,
    BlogListDatesIndexPage,
    BlogListTagsIndexPage,
    BlogPage,
    BlogPageTag,
    BlogTagIndexPage,
)
from coldcms.blog.views import (
    refresh_authors,
    refresh_dates,
    refresh_index_pages,
    refresh_tags,
)
from django.contrib.auth.models import User
from factory.django import mute_signals
from taggit.models import Tag
from wagtail.core.signals import page_published, page_unpublished

pytestmark = pytest.mark.django_db

call_count = 0


def fake_refresh():
    global call_count
    call_count += 1


def noop(*_):
    pass


def test_refresh_index_pages(monkeypatch):
    global call_count
    call_count = 0
    monkeypatch.setattr(views, "refresh_tags", fake_refresh)
    monkeypatch.setattr(views, "refresh_authors", fake_refresh)
    monkeypatch.setattr(views, "refresh_dates", fake_refresh)
    refresh_index_pages(BlogPage)
    assert call_count == 3
    call_count = 0


def test_refresh_index_pages_not_called(monkeypatch):
    global call_count
    call_count = 0
    monkeypatch.setattr(views, "refresh_tags", fake_refresh)
    monkeypatch.setattr(views, "refresh_authors", fake_refresh)
    monkeypatch.setattr(views, "refresh_dates", fake_refresh)
    refresh_index_pages(dict)
    assert call_count == 0


def test_refresh_dates(monkeypatch):
    dates = [
        datetime.date(year=2019, month=month, day=1) for month in range(1, 13)
    ]
    BlogIndexPage.objects.create(
        title="index", intro="intro", depth=1, path="0004"
    )
    for i, date in enumerate(dates[4:]):
        BlogPage.objects.create(
            title="My post1",
            path=f"001{i+1}",
            depth=1,
            date=date,
            body="so much contents",
        )

    for i, date in enumerate(dates[:-4]):
        BlogDateIndexPage.objects.create(
            title="My post1", path=f"002{i+1}", depth=1, date=date
        )

    assert BlogListDatesIndexPage.objects.first() is None
    with mute_signals(page_published, page_unpublished):
        refresh_dates()
    assert BlogListDatesIndexPage.objects.first() is not None
    assert BlogDateIndexPage.objects.count() == 8
    assert {
        index_page.date for index_page in BlogDateIndexPage.objects.all()
    } == set(dates[4:])


def test_refresh_authors(monkeypatch):
    authors = [User.objects.create(username=f"user{i}") for i in range(1, 13)]
    BlogIndexPage.objects.create(
        title="index", intro="intro", depth=1, path="0004"
    )
    now = datetime.datetime.now()
    for i, author in enumerate(authors[4:]):
        BlogPage.objects.create(
            title="My post1",
            path=f"001{i+1}",
            depth=1,
            date=now,
            body="so much contents",
            owner=author,
        )

    for i, author in enumerate(authors[:-4]):
        BlogAuthorIndexPage.objects.create(
            title="My post1", path=f"002{i+1}", depth=1, author=author
        )

    assert BlogListAuthorsIndexPage.objects.first() is None
    with mute_signals(page_published, page_unpublished):
        refresh_authors()
    assert BlogListAuthorsIndexPage.objects.first() is not None
    assert BlogAuthorIndexPage.objects.count() == 8
    assert {
        index_page.author for index_page in BlogAuthorIndexPage.objects.all()
    } == set(authors[4:])


def test_refresh_tags(monkeypatch):
    tags = [Tag.objects.create(name=f"tag{i}") for i in range(1, 13)]
    BlogIndexPage.objects.create(
        title="index", intro="intro", depth=1, path="0004"
    )
    now = datetime.datetime.now()
    for i, tag in enumerate(tags[4:]):
        blog_page = BlogPage.objects.create(
            title="My post1",
            path=f"001{i+1}",
            depth=1,
            date=now,
            body="so much contents",
        )
        BlogPageTag.objects.create(tag=tag, content_object=blog_page)

    for i, tag in enumerate(tags[:-4]):
        BlogTagIndexPage.objects.create(
            title="My post1", path=f"002{i+1}", depth=1, tag=tag
        )

    assert BlogListTagsIndexPage.objects.first() is None
    with mute_signals(page_published, page_unpublished):
        refresh_tags()
    assert BlogListTagsIndexPage.objects.first() is not None
    assert BlogTagIndexPage.objects.count() == 8
    assert {
        index_page.tag for index_page in BlogTagIndexPage.objects.all()
    } == set(tags[4:])
