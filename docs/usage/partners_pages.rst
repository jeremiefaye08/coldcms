Partners page
=============

A partners page is designed to simply list partners, with their logo, name and website.

.. image:: ../_static/images/partners_page_edit(1).png
    :alt:

.. image:: ../_static/images/partners_page_edit(2).png
    :alt:

.. image:: ../_static/images/partners_page_edit(3).png
    :alt:

You can sort the partners within categories. If you don't want to use categories, you can create only one category, without a name, and put all of your partners in there.

The logo is displayed above the partner's name, and they are both clickable, redirecting to the partner's website (if a website URL has been provided).

The partners page looks like this:

.. image:: ../_static/images/partners_page_layout.png
    :alt: