import pytest
from coldcms.blocks.blocks import CTABlock
from coldcms.generic_page.models import GenericPage

pytestmark = pytest.mark.django_db


def test_link_struct_value_external_url():
    home = GenericPage.objects.create(title="My Home", path="/path/", depth=666)
    cta = CTABlock()
    link_value = cta.to_python(
        {"text": "blablabla", "custom_url": "https://lalala.com", "page": home.pk,}
    )
    assert link_value.url() == "https://lalala.com"


def test_link_struct_value_page():
    home = GenericPage.objects.create(title="My Home", path="/path/", depth=666)
    home.url_path = "/home/"
    home.save()
    cta = CTABlock()
    link_value = cta.to_python({"text": "blablabla", "page": home.pk})
    assert link_value.url() == home.url


def test_link_struct_value_extra_url():
    home = GenericPage.objects.create(title="My Home", path="/path/", depth=666)
    cta = CTABlock()
    link_value = cta.to_python(
        {
            "text": "blablabla",
            "custom_url": "https://lalala.com/",
            "page": home.pk,
            "extra_url": "#blabla",
        }
    )
    assert link_value.url() == "https://lalala.com/#blabla"


def test_link_struct_value_no_url():
    cta = CTABlock()
    link_value = cta.to_python({"text": "blablabla", "extra_url": "#blabla"})
    assert link_value.url() == "#blabla"


def test_link_struct_value_text_page():
    home = GenericPage.objects.create(title="My Home", path="/path/", depth=666)
    cta = CTABlock()
    link_value = cta.to_python({"page": home.pk})
    assert link_value.text() == home.title


def test_link_struct_value_text_url():
    cta = CTABlock()
    link_value = cta.to_python({"custom_url": "https://lalala.com/"})
    assert link_value.text() == "https://lalala.com/"


def test_link_struct_value_text():
    home = GenericPage.objects.create(title="My Home", path="/path/", depth=666)
    cta = CTABlock()
    link_value = cta.to_python({"text": "blablabla", "page": home.pk})
    assert link_value.text() == "blablabla"
