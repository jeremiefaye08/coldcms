Managing the menu
=================

By default, the menu bar appears at the top of each page. It contains links to the different pages of the website, as well as eventual external links.

Some options are available to edit this menu:

    .. toctree::
        :maxdepth: 2

        menu_options
        dropdown
        links
        social_media_icons