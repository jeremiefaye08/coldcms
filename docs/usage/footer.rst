Adding a footer
===============

To add a footer, go into the menu **'Settings'**, then **'Footer'**.

.. image:: ../_static/images/footer_add(1).png
    :alt:

There you can add several footer columns. Each column contains several links to either internal or external pages. A link to an internal page will open in the same tab (and replace the current page), a link to an external website will open in a new tab.

You can give names to your columns, e.g. to sort the links into categories, or you can just use the columns for display, without naming them. 

.. image:: ../_static/images/footer_add(2).png
    :alt:

|

A footer with 3 columns looks like this:

.. image:: ../_static/images/footer.png
    :alt:


You can also add social media icons in your footer.

.. image:: ../_static/images/footer_icons(1).png
    :alt:

Click on **'Social media icon'**, then fill in the icon type, the link to your social media, and the text you want to appear as a link next to the social media icon.

.. image:: ../_static/images/footer_icons(2).png
    :alt:

Social media icons in the footer look like this:

.. image:: ../_static/images/footer_icons(3).png
    :alt:

More information on social media icons in **Managing the menu / Add social media icons**.