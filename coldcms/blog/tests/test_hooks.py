from datetime import datetime

import pytest
from coldcms.blog.models import BlogListTagsIndexPage, BlogPage
from coldcms.blog.wagtail_hooks import dont_show_index_pages
from wagtail.core.models import Page

pytestmark = pytest.mark.django_db


def test_dont_show_index_pages():
    existing_pages = list(Page.objects.all().values_list("id", flat=True))
    blog_page = BlogPage.objects.create(
        title="My post1",
        path="0002",
        depth=1,
        date=datetime.utcnow(),
        body="so much contents",
    )
    BlogListTagsIndexPage.objects.create(
        title="Blog index", path="0003", depth=1
    )
    pages = dont_show_index_pages(
        None, Page.objects.all().exclude(id__in=existing_pages), None
    )
    assert len(pages) == 1
    assert pages[0].specific == blog_page
