Add social media icons
======================

In the menu bar, you can add links to your social media as icons (see in section **Adding a footer** to add them in the footer).

Go to the menu **'Settings'**, then **'Menu options'**. Click on **'Social media icon'** to add one. You can add as many as you want, although ColdCMS provides icons for 9 social media for now (list below).

.. image:: ../_static/images/menu_icons_edit(1).png
    :alt:

Fill in the icon type, the link to your social media, and the text you want to appear as a link next to the social media icon.

.. image:: ../_static/images/menu_icons_edit(2).png
    :alt:

The social media icons available are:

    - Facebook
    - Twitter
    - Instagram
    - Youtube
    - Pinterest
    - LinkedIn
    - GitLab
    - GitHub
    - Tumblr

A social media icon in the menu bar looks like this:

.. image:: ../_static/images/menu_icons(1).png
    :alt:

And in mobile version:

.. image:: ../_static/images/menu_icons(2).png
    :alt:

You can choose to add the icon only, without any text, but be aware that **some social media companies require a text beside their logo**. You can find information on their respective branding websites.

It is not advised to use many social media icons with long text in the menu bar. Instead, you can also add them in the footer. See in section **Adding a footer**.

A menu bar with the icons without text looks like this:

.. image:: ../_static/images/menu_icons(3).png
    :alt:

And in mobile version:

.. image:: ../_static/images/menu_icons(4).png
    :alt: