# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2020, Pauline Schmitt, Hugo Delval, Arthur Vuillard, Lucien
# Deleu
# This file is distributed under the same license as the ColdCMS package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: ColdCMS 0.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-05-28 16:34+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: ../../usage/simple_pages.rst:2 3e8e8ae63f7b440e8f68e089a8221356
msgid "Simple page"
msgstr "Page simple"

#: ../../usage/simple_pages.rst:4 1efba11f8fe943e2a3a02bffa64ad8ff
msgid ""
"A simple page has a very basic layout: a title, and a text field, in "
"which you can add images, videos or documents. Read more about rich text "
"fields in `Wagtail's documentation "
"<https://docs.wagtail.io/en/stable/editor_manual/new_pages/creating_body_content.html"
"#rich-text-fields>`_."
msgstr ""
"Une page simple a une structure très basique : un titre, et un champ de "
"texte, dans lequel vous pouvez ajouter des images, vidéos ou documents. "
"En savoir plus sur les Rich Text Fields dans la `documentation de Wagtail"
" "
"<https://docs.wagtail.io/en/stable/editor_manual/new_pages/creating_body_content.html"
"#rich-text-fields>`_ (en anglais)."

#: ../../usage/simple_pages.rst:6 cba12fd4068a48809a0438d3cd5e31b0
msgid "It can be used for a legal notice, or any simple information display."
msgstr ""
"Une page simple peut être utilisée pour les mentions légales, ou pour "
"n'importe quel affichage sobre d'information. "

#: ../../usage/simple_pages.rst:9 46a5cfad03224a4bb5f732d8155a355c
msgid ".. image:: usage/../_static/images/simple_page_edit.png"
msgstr ""

#: ../../usage/simple_pages.rst:10 b768019d098140a68e93dfb971c70c4c
msgid "A simple page looks like:"
msgstr "Voilà à quoi ressemble une page simple : "

#: ../../usage/simple_pages.rst:13 6ce72591aa7d4a3aaf1751a7384cffb4
msgid ".. image:: usage/../_static/images/simple_page_layout.png"
msgstr ""

