from coldcms.blocks.blocks import CTABlock
from colorfield.fields import ColorField
from colorfield.widgets import ColorWidget
from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _
from modelcluster.fields import ParentalKey
from modelcluster.models import ClusterableModel
from wagtail.admin.edit_handlers import (
    FieldPanel,
    InlinePanel,
    MultiFieldPanel,
    StreamFieldPanel,
)
from wagtail.contrib.settings.models import BaseSetting
from wagtail.contrib.settings.registry import register_setting
from wagtail.core.blocks import CharBlock, ChoiceBlock, StructBlock, URLBlock
from wagtail.core.fields import StreamField
from wagtail.core.models import Orderable
from wagtail.documents.edit_handlers import DocumentChooserPanel
from wagtail.images.edit_handlers import ImageChooserPanel


class SocialMediaIconBlock(StructBlock):
    icons_list = [
        ("facebook", "Facebook"),
        ("twitter", "Twitter"),
        ("github", "GitHub"),
        ("gitlab", "GitLab"),
        ("linkedin", "LinkedIn"),
        ("instagram", "Instagram"),
        ("youtube", "YouTube"),
        ("pinterest", "Pinterest"),
        ("tumblr", "Tumblr"),
    ]
    icon_type = ChoiceBlock(
        label=_("Icon type"),
        icon="site",
        blank=False,
        default=icons_list[0],
        choices=icons_list,
        help_text=_(
            "Which social media is the URL linking to (will display the corresponding icon)"
        ),
    )
    link = URLBlock(
        label=_("Link"), icon="link", help_text=_("URL to your social media")
    )
    link_text = CharBlock(
        label=_("Link text"),
        required=False,
        max_length=40,
        help_text=_(
            'The text displayed next to the social media icon. Should be of the type "Follow us on ...". Be aware that some social media companies require that this text appears next to their logo. Make sure by checking their branding website.'
        ),
    )

    class Meta:
        icon = "site"
        verbose_name = _("Social media icon")
        label = _("Social media icon")


@register_setting(icon="code")
class Footer(BaseSetting, ClusterableModel):
    """A footer containing a list of footer columns"""

    icons = StreamField(
        [("social_media_icon", SocialMediaIconBlock())],
        blank=True,
        null=True,
        verbose_name=_("Social media icons"),
    )

    panels = [
        MultiFieldPanel(
            [InlinePanel("columns", label=_("Footer columns")),],
            heading=_("Footer columns"),
            classname="collapsible collapsed",
        ),
        MultiFieldPanel(
            [StreamFieldPanel("icons"),],
            heading=_("Social media icons"),
            classname="collapsible collapsed",
        ),
    ]

    def __str__(self):
        return "Footer"

    class Meta:
        verbose_name_plural = _("Footer")
        verbose_name = _("Footer")


class FooterColumn(Orderable):
    footer = ParentalKey("site_settings.Footer", related_name="columns")
    title = models.CharField(
        max_length=40, verbose_name=_("Title"), blank=True, null=True
    )
    links = StreamField(
        [("links", CTABlock(icon="link"))],
        blank=True,
        null=True,
        verbose_name=_("Links"),
    )

    panels = [FieldPanel("title"), StreamFieldPanel("links")]


@register_setting(icon="image")
class SiteSettings(BaseSetting):
    """Site settings"""

    logo = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
        verbose_name=_("Logo"),
    )

    panels = [ImageChooserPanel("logo")]

    class Meta:
        verbose_name = _("Logo")
        verbose_name_plural = _("Logos")


@register_setting(icon="doc-full-inverse")
class CSSStyleSheet(BaseSetting):
    """Load a CSS stylesheet"""

    CSS_stylesheet = models.ForeignKey(
        "wagtaildocs.Document",
        null=True,
        blank=True,
        help_text=_(
            "Upload your own CSS stylesheet to custom the appearance of your website"
        ),
        on_delete=models.SET_NULL,
        related_name="+",
        verbose_name=_("CSS"),
    )

    panels = [DocumentChooserPanel("CSS_stylesheet")]

    class Meta:
        verbose_name = _("CSS stylesheet")
        verbose_name_plural = _("CSS stylesheets")


@register_setting(icon="collapse-down")
class MenuOptions(BaseSetting):
    """Option to show or hide the menu bar"""

    hide_menu = models.BooleanField(
        verbose_name=_("Hide Menu"),
        default=False,
        help_text=_("Whether to hide the menu bar or not"),
    )

    icons = StreamField(
        [("social_media_icon", SocialMediaIconBlock())],
        blank=True,
        null=True,
        verbose_name=_("Social media icons"),
    )

    panels = [
        FieldPanel("hide_menu"),
        MultiFieldPanel([StreamFieldPanel("icons"),], heading=_("Social media icons"),),
    ]

    class Meta:
        verbose_name = _("Menu options")


@register_setting(icon="edit")
class VariablesColors(BaseSetting):

    black = ColorField(default=settings.BLACK, verbose_name=_("Noire"))
    black_bis = ColorField(default=settings.BLACK_BIS, verbose_name=_("Second noire"))
    black_ter = ColorField(default=settings.BLACK_TER, verbose_name=_("Troisième noire"))

    grey_darker = ColorField(default=settings.GREY_DARKER, verbose_name=_("Gris très foncé"))
    grey_dark = ColorField(default=settings.GREY_DARK, verbose_name=_("Gris foncé"))
    grey = ColorField(default=settings.GREY, verbose_name=_("Gris"))
    grey_light = ColorField(default=settings.GREY_LIGHT, verbose_name=_("Gris clair"))
    grey_lighter = ColorField(default=settings.GREY_LIGHTER, verbose_name=_("Gris très clair"))
    grey_lightest = ColorField(default=settings.GREY_LIGHTEST, verbose_name=_("Gris le plus clair"))

    white = ColorField(default=settings.WHITE, verbose_name=_("Blanc"))
    white_bis = ColorField(default=settings.WHITE_BIS, verbose_name=_("Second blanc"))
    white_ter = ColorField(default=settings.WHITE_TER, verbose_name=_("Troisième blanc"))

    orange = ColorField(default=settings.ORANGE, verbose_name=_("Orange"))
    yellow = ColorField(default=settings.YELLOW, verbose_name=_("Jaune"))
    green = ColorField(default=settings.GREEN, verbose_name=_("Vert"))
    turquoise = ColorField(default=settings.TURQUOISE, verbose_name=_("Turquoise"))
    cyan = ColorField(default=settings.CYAN, verbose_name=_("Cyan"))
    blue = ColorField(default=settings.BLUE, verbose_name=_("Bleu"))
    purple = ColorField(default=settings.PURPLE, verbose_name=_("Violet"))
    red = ColorField(default=settings.RED, verbose_name=_("Rouge"))

    primary = ColorField(default=settings.PRIMARY, verbose_name=_("Principale"))
    info = ColorField(default=settings.INFO, verbose_name=_("Info"))
    success = ColorField(default=settings.SUCCESS, verbose_name=_("Succès"))
    warning = ColorField(default=settings.WARNING, verbose_name=_("Attention"))
    danger = ColorField(default=settings.DANGER, verbose_name=_("Erreur"))

    panels = [
        FieldPanel("black", widget=ColorWidget),
        FieldPanel("black_bis", widget=ColorWidget),
        FieldPanel("black_ter", widget=ColorWidget),

        FieldPanel("grey_darker", widget=ColorWidget),
        FieldPanel("grey_dark", widget=ColorWidget),
        FieldPanel("grey", widget=ColorWidget),
        FieldPanel("grey_light", widget=ColorWidget),
        FieldPanel("grey_lighter", widget=ColorWidget),
        FieldPanel("grey_lightest", widget=ColorWidget),

        FieldPanel("white_ter", widget=ColorWidget),
        FieldPanel("white_bis", widget=ColorWidget),
        FieldPanel("white", widget=ColorWidget),

        FieldPanel("orange", widget=ColorWidget),
        FieldPanel("yellow", widget=ColorWidget),
        FieldPanel("green", widget=ColorWidget),
        FieldPanel("turquoise", widget=ColorWidget),
        FieldPanel("cyan", widget=ColorWidget),
        FieldPanel("blue", widget=ColorWidget),
        FieldPanel("purple", widget=ColorWidget),
        FieldPanel("red", widget=ColorWidget),

        FieldPanel("primary", widget=ColorWidget),
        FieldPanel("info", widget=ColorWidget),
        FieldPanel("success", widget=ColorWidget),
        FieldPanel("warning", widget=ColorWidget),
        FieldPanel("danger", widget=ColorWidget),
    ]

    class Meta:
        verbose_name = _("Couleurs")
