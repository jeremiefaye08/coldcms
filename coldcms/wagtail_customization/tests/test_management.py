import io

import pytest
from coldcms.generic_page.models import GenericPage
from django.core import management
from wagtail.images.models import Image

pytestmark = pytest.mark.django_db


def test_setup_initial_data():
    assert GenericPage.objects.count() == 0
    assert Image.objects.count() == 0
    management.call_command("setup_initial_data", "--noconfirm")
    assert GenericPage.objects.count() == 1
    assert Image.objects.count() == 1


def test_setup_initial_data_already_existing_data():
    assert GenericPage.objects.count() == 0
    assert Image.objects.count() == 0
    management.call_command("setup_initial_data", "--noconfirm")
    assert GenericPage.objects.count() == 1
    assert Image.objects.count() == 1
    management.call_command("setup_initial_data", "--noconfirm")
    assert GenericPage.objects.count() == 1
    assert Image.objects.count() == 1


def test_setup_initial_data_stdin(monkeypatch):
    assert GenericPage.objects.count() == 0
    assert Image.objects.count() == 0
    monkeypatch.setattr("sys.stdin", io.StringIO("y"))
    management.call_command("setup_initial_data")
    assert GenericPage.objects.count() == 1
    assert Image.objects.count() == 1


def test_setup_initial_data_stdin_cancel(monkeypatch):
    assert GenericPage.objects.count() == 0
    assert Image.objects.count() == 0
    monkeypatch.setattr("sys.stdin", io.StringIO("n"))
    management.call_command("setup_initial_data")
    assert GenericPage.objects.count() == 0
    assert Image.objects.count() == 0


def test_setup_initial_data_language_fr(monkeypatch):
    monkeypatch.setenv("LANG", "fr_FR")
    management.call_command("setup_initial_data", "--noconfirm")
    home = GenericPage.objects.first()
    home_title = home.content_blocks[0].value[0].get("title")
    assert home_title == "Bienvenue sur votre instance ColdCMS"


def test_setup_initial_data_language_en(monkeypatch):
    monkeypatch.setenv("LANG", "en_EN")
    management.call_command("setup_initial_data", "--noconfirm")
    home = GenericPage.objects.first()
    home_title = home.content_blocks[0].value[0].get("title")
    assert home_title == "Welcome to your ColdCMS instance"
