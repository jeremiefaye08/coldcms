Adding a logo
=============

You can add a logo through the menu **'Settings'** and then **'Logo'**.

.. image:: ../_static/images/logo_edit(1).png
    :alt:

Then pick an image to be used as your logo. For further documentation, please read `Wagtail documentation about inserting images <https://docs.wagtail.io/en/stable/editor_manual/new_pages/inserting_images.html>`_.

.. image:: ../_static/images/logo_edit(2).png
    :alt:

The logo will appear at the left of the menu bar, and in the tab icon. The logo in the menu is clickable, and redirects to the home page.

.. image:: ../_static/images/logo_in_menu.png
    :alt:

.. image:: ../_static/images/logo_in_favicon.png 
    :alt: