# Generated by Django 3.0.4 on 2020-06-30 09:10

from django.db import migrations
import wagtail.core.blocks
import wagtail.core.fields


class Migration(migrations.Migration):

    dependencies = [
        ('faq', '0004_auto_20200422_1259'),
    ]

    operations = [
        migrations.AlterField(
            model_name='faqpage',
            name='questions_groups',
            field=wagtail.core.fields.StreamField([('questions_groups', wagtail.core.blocks.StructBlock([('category_name', wagtail.core.blocks.CharBlock(help_text='The type of question (ex: Accessibility, Rules). You can keep this field empty if you only have one category of questions/answers', label='Category name', max_length=100, required=False)), ('questions', wagtail.core.blocks.StreamBlock([('questions', wagtail.core.blocks.StructBlock([('question', wagtail.core.blocks.CharBlock(label='Question', max_length=250)), ('answer', wagtail.core.blocks.RichTextBlock(features=['bold', 'italic', 'link', 'document-link', 'ol', 'ul', 'hr'], label='Answer'))]))], label='Questions'))]))], blank=True, null=True, verbose_name='Question groups'),
        ),
    ]
