import pytest
from coldcms.wagtail_customization.wagtail_hooks import (
    register_button_regenerate_statics,
)

pytestmark = pytest.mark.django_db


def test_register_statics_menu_item():
    class MockPage:
        pk = 1

    menu_item = register_button_regenerate_statics(MockPage(), None)
    assert 'href="/admin/generate_statics' in str(list(menu_item)[0])
