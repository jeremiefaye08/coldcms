# Generated by Django 3.0.4 on 2020-04-02 13:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('site_settings', '0002_auto_20190725_1326'),
    ]

    operations = [
        migrations.AlterField(
            model_name='footercolumn',
            name='title',
            field=models.CharField(blank=True, max_length=40, null=True, verbose_name='Title'),
        ),
    ]
