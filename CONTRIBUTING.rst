============================
How to contribute to ColdCMS
============================

ColdCMS is an open source project. Any contribution is welcome, provided that it follows the guidelines given in this document.

Also, please read and follow our `Code of Conduct <https://gitlab.com/hashbangfr/coldcms/-/blob/master/CODE_OF_CONDUCT.rst>`_.


1. Dev install
==============

Clone the project, then follow the section **Dev** of `README <https://gitlab.com/hashbangfr/coldcms/-/blob/master/README.rst#dev>`_.


2. Code formatting
==================

Code is simpler to review if we all use the same coding style :)

For ColdCMS we used `black <https://github.com/psf/black>`_ and `flake8 <https://flake8.pycqa.org/en/latest/>`_ for Python code formatting, and `isort <https://github.com/timothycrosley/isort>`_ to automatically sort imports.

**Install**

.. code-block:: shell

    pip install black  # requires Python >= 3.6
    pip install isort
    pip install flake8


**Usage**

.. code-block:: shell

    black --line-length=88 <path/to_file>
    isort -rc -w 88 -m 3 <path/to_file>


The tests will automatically run a flake8 and isort check, with parameters specified in `setup.cfg <https://gitlab.com/hashbangfr/coldcms/-/blob/master/setup.cfg#L63-75>`_.


3. Merge requests
=================

If you resolve an existing issue :

* Create one branch per issue

* Name the commit ``Fix <issue_url>``


4. Feature requests and bug reports
===================================

If you want to suggest a contribution :

* Open an issue

* Label it correctly : ``bugs`` or ``feature requests``


5. Tests
========

Before committing, please run ``pytest`` and make sure all the tests pass before pushing your code.

If you add a feature, please write the needed unit tests in the folder ``/tests/`` of the modified app.
There is a coverage check, if a test is missing, the gitlab pipeline will fail.


6. Documentation
================

If you make a merge request for a feature that impacts ColdCMS usage, you are advised to add the corresponding documentation.

The documentation for ColdCMS is generated with Sphinx and ReadtheDocs. To update it :

1. Update the files or add a new file in /docs/usage. The documentation files are in .rst format, see `how to use reStructuredText <https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html>`_.
2. If you want to test the rendered doc locally, run ``make html`` in /docs, and check the output by opening ``/docs/_build/html/index.html`` in a web browser.
3. Once you are happy with it, include the documentation in your merge request.


7. Translation
==============

Steps to translate ColdCMS components
-------------------------------------

    1. Make sure to mark as translatable the strings meant to be displayed to the user. Usually, for that, we import gettext as follows

    .. code-block:: python

        from django.utils.translation import ugettext_lazy as _

    and we mark the translatable strings like this:

    .. code-block:: python

        _("UI String to be translated")

    2. To update the ``django.po`` file containing all the strings to translate, run from the project root (same level as ``manage.py``):

    .. code-block:: shell

        python manage.py makemessages -a

    3. Write the needed translations in ``coldcms/locale/$LANG/LC_MESSAGES/django.po``, where ``$LANG`` is the language (for example ``fr`` for French).

    4. Compile your translations with:

    .. code-block:: shell

        python manage.py compilemessages

    5. Check the output by configuring your web browser in the translation language.

    6. Once you are happy with the translation, include the ``django.po`` file in your merge request.


Steps to translate documentation
--------------------------------

    1. Install Sphinx internationalization tool 

    .. code-block:: shell
    
        pip install sphinx-intl

    2. To update the documentation to be translated, run in ``docs/``

    .. code-block:: shell

        make gettext    
        sphinx-intl update -p _build/gettext -l <lang>

    where ``<lang>`` is the language code, for example ``fr``.

    3. Write the translations in ``.po`` files, in ``docs/locale/$LANG/LC_MESSAGES``, where ``$LANG`` is the language (for example ``fr``).

        Note: some ``#, fuzzy`` might have been added, this means a pre-existing translation has slightly changed. Make sure the translation is still accurate, and then remove the ``#, fuzzy`` line.

    4. Build the translated documentation

    .. code-block:: shell
    
        sphinx-build -b html -D language=<lang> . _build/html/<lang>

    where ``<lang>`` is the language code, for example ``fr``.
    Instead, you can also run in the project root:

    .. code-block:: shell

        python manage.py compilemessages

    5. Check the output by opening ``docs/_build/html/<lang>/index.html`` (replace <lang> with the language code).

    6. Once you are happy with the translation, include the ``.po`` files in your merge request.


**To replace an image in translation (e.g. a screenshot of UI in another language)**

If you want to replace the image ``docs/_static/images/image_name.png`` by another one in the translated version of the documentation, just save your image as ``docs/_static/images/<lang>/image_name_<lang>.png``.
For example, ``docs/_static/images/blog_index_edit.png`` is replaced in the French translation by ``docs/_static/images/fr/blog_index_edit_fr.png``.

In ``.po`` files, images are included as translatable content. If the image has an alternative text, please translate it in your language (useful for screen readers). Leave the image path as it is, just replace the :alt: text.
For example, in ``docs/locale/fr/LC_MESSAGES/usage/links.po``:

.. code-block:: python

    msgid ""
    ".. image:: usage/../_static/images/links.png\n"
    "   :alt: the link appears in the menu bar"
    msgstr ""
    ".. image:: usage/../_static/images/links.png\n"
    "   :alt: le lien apparaît dans la barre de menu"


If the image doesn't have an alternative text, just leave the translation blank:

.. code-block:: python

    msgid ".. image:: usage/../_static/images/links.png\n"
    msgstr ""


**To translate UI elements of the documentation**

**Skip steps 1 to 3** if your language already exists in ``docs/locale/``.

    1. Install Babel:

    .. code-block:: shell

        pip install Babel

    2. To generate ``sphinx-rtd-theme.pot``, run at the project root:
    
    .. code-block:: shell
        
        python setup.py extract_messages 

    3. Create the ``.po`` file for your language using ``msgmerge`` tool, and place it in your project as ``locale/$LANG/LC_MESSAGES/sphinx.po`` (with ``$LANG`` the language code).

    4. Add translations manually in ``docs/locale/$LANG/LC_MESSAGES/sphinx.po``.

    5. Apply translations

    .. code-block:: python
    
        python manage.py compilemessages
    
See `this github issue <https://github.com/readthedocs/sphinx_rtd_theme/issues/403#issuecomment-289096940>`_ for further details on translating UI elements of documentation using sphinx-rtd-theme.