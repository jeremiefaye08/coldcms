Add an internal or external link
================================

Links will appear in the menu bar, and redirect to another page (either internal or external to the website). A link to an internal page will open in the same tab (and replace the current page), a link to an external website will open in a new tab.

.. image:: ../_static/images/links_edit.png
    :alt: a link has a title, an internal or external URL, and a 'Show in menus' checkbox

The link will look like this:

.. image:: ../_static/images/links.png
    :alt: the link appears in the menu bar