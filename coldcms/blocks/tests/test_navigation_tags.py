import pytest
from coldcms.blocks.templatetags.navigation_tags import get_site_root, top_menu
from coldcms.generic_page.models import GenericPage
from coldcms.simple_page.models import SimplePage
from wagtail.core.models import Site

pytestmark = pytest.mark.django_db


def test_get_site_root():
    home = GenericPage.objects.create(title="My Home", path="/path/", depth=666)
    site = Site.objects.create(root_page=home)

    class Request:
        pass

    request = Request()
    request.site = site
    context = {"request": request}
    assert get_site_root(context).pk == home.pk


def test_top_menu():
    home = GenericPage.objects.create(
        title="My Home", path="/path/", depth=666, numchild=1
    )
    simple_page = SimplePage.objects.create(
        title="Notice", content="Careful", path="/path/2", depth=667
    )
    context = top_menu(home)
    items = context["menuitems"]
    assert not items[0].show_dropdown
    assert items[0].pk == simple_page.pk
