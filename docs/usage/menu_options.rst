Show/Hide the menu bar
======================

By default, each page created appears in the menu bar.

If you want to remove a page from the menu bar:

    - When editing the page, go into the **'Settings'** tab, uncheck the box **'Show in menus'** (checked by default), and publish the page.

    .. image:: ../_static/images/show_in_menus.png
        :alt:

If you want to hide the entire menu bar:

    - Go into the menu **'Settings'** and then **'Menu options'**.

    .. image:: ../_static/images/menu_options_edit(1).png
        :alt:

    |

    - Check the box **'Hide menu'** (unchecked by default), and save.

    .. image:: ../_static/images/menu_options_edit(2).png
        :alt:

Example of a page with a menu bar:

.. image:: ../_static/images/menu_options_with_menu.png
    :alt:

The same page without the menu bar:

.. image:: ../_static/images/menu_options_without_menu.png
    :alt:
