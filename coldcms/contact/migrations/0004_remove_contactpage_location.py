# Generated by Django 3.0.4 on 2020-06-15 09:38

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contact', '0003_auto_20190725_1326'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contactpage',
            name='location',
        ),
    ]
